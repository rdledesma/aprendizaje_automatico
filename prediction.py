# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 17:44:27 2020

@author: Dario Ledesma
"""


# Regresion con redes neuronales
import numpy
from pandas import read_csv
from keras.models import Sequential
from keras.layers import Dense
from keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import KFold
# cargamos los datos


dataset = read_csv('datos.csv')


X = dataset.iloc[:,2:4].values
y = dataset.iloc[:,4].values




#dividir el dataset en conjunto de entrenamiento y conjunto de testing
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)



dataframe = read_csv("datos.csv", header=0)
dataset = dataframe.values
# dividimos los datos entre los inputs y variables
X = dataset[:,2:4]
Y = dataset[:,4]
# se define el modelo base
def baseline_model():
  # creamos el modelo
  model = Sequential()
  model.add(Dense(4, input_dim=2, kernel_initializer='normal', activation='relu'))
  model.add(Dense(1, kernel_initializer='normal'))
  # Compilamos el modelo
  model.compile(loss='mean_squared_error', optimizer='adam')
  
  model.fit(X_train, y_train, batch_size=10, epochs=500)
  
  y_pred = model.predict(X_test)
  
  print(y_pred)
  return model



baseline_model()


