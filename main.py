# -*- coding: utf-8 -*-
"""
Created on Sat Mar 21 20:56:47 2020

@author: Dario Ledesma
"""

import numpy as np
import pandas as pd

#importar el data set
dataset = pd.read_csv('datos.csv')


X = dataset.iloc[:,2:4].values
y = dataset.iloc[:,4].values




#dividir el dataset en conjunto de entrenamiento y conjunto de testing
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

#Parte 2 - Construir la RNA

import keras
from keras.models import Sequential
from keras.layers import Dense

#Inicializar la RNA
classifier = Sequential()

#Añadir las capas de entradas y primera capa oculta
classifier.add(Dense(units=2, kernel_initializer="uniform", activation="relu", input_dim=2))

#Añadir la segunda capa oculta
classifier.add(Dense(units=2, kernel_initializer="uniform", activation="relu"))

#añadir la capa de salida
classifier.add(Dense(1, kernel_initializer="uniform", activation="softmax"))

#compilar la RNA
classifier.compile(optimizer = "adam", loss='mean_squared_error')

#Ajustar la RNA al conjunto de entrenamiento
classifier.fit(X_train, y_train, batch_size=10, epochs=100)

    
#Parte 3 Evaluar el modelo y calcular predicciones finales

y_pred = classifier.predict(X_test)


